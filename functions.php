<?php 

function load_title_tag(){
  //chamar a tag title
  add_theme_support('title-tag');

  //Logo
  add_theme_support('custom-logo');
}
add_action('after_setup_theme','load_title_tag');

// Previnir o erro na tag Title em versões antigas
if (!function_exists('_wp_render_title_tag')) {
  function load_render_title() {
    ?>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <?php
  }
  add_action('wp_head', 'load_render_title');
}

//WP_Bootstrap_Navwalker
require_once get_template_directory().'/wp-bootstrap-navwalker.php';

//Registro de Menus
register_nav_menus(array(
  'principal' => __('Menu Principal','agames')
));

//Miniatura dos Posts
add_theme_support('post-thumbnails');
set_post_thumbnail_size(1280, 720, true);

//Tamanho do Resumo
add_filter('excerpt_length',function($length){
  return 20;
});

//Botão da Paginação
add_filter('next_posts_link_attributes','posts_link_attributes');
add_filter('previous_posts_link_attributes','posts_link_attributes');

function posts_link_attributes(){
  return 'class="btn btn-outline-cor-3"';
}

//Barra Lateral - Sidebar
register_sidebar(
  array(
    'name' => 'Barra lateral',
    'id' => 'sidebar',
    'before_widget' => '<div class="card mb-4">',
    'after_widget' => '</div></div>',
    'before_title' => '<h5 class="card-header">',
    'after_title' => '</h5><div class="card-body custom-link">',
));

//Ativar formulário dos comentários
function load_comments_js(){
  if ( (!is_admin()) && is_singular() && comments_open() && get_option('thread_comments') ) wp_enqueue_script('comment-reply');
}
add_action('wp_print_scripts', 'load_comments_js');

// Personalizar os comentários
function format_comment($comment, $args, $depth) {

  $GLOBALS['comment'] = $comment; ?>

  <div <?php comment_class('ml-4'); ?> id="comment-<?php comment_ID(); ?>">

    <div class="card mb-3">
      <div class="card-body">

      <div class="comment-intro">

        <h5 class="card-title"><?php printf(__('%s'), get_comment_author_link()) ?></h5>
        <h6 class="card-subtitle mb-3 text-muted">Comentou em <?php printf(__('%1$s'), get_comment_date('d/m/y'), get_comment_time()) ?></h6>
    
      </div>

      <?php comment_text(); ?>

      <div class="reply">
        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>

      </div>
    </div>

  <?php

}

// Incluir as funções de personalização
require get_template_directory(). '/inc/customizer.php';

?>