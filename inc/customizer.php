<?php 

  function load_customize_register($wp_customize) {
  
    //Rodapé
    $wp_customize -> add_section('footer', array(
      'title' => __('Rodapé', 'Agames'),
      'description' => sprintf(__('Opções para o rodapé','Agames')),
      'priority' => 20
    ));

    $wp_customize -> add_setting('footer_title', array(
      'default' => _x('Diego Capella', 'Agames'),
      'type' => 'theme_mod'
    ));

    $wp_customize -> add_control('footer_title',array(
      'label' => __('Autor do Tema', 'Diego Capella'),
      'section' => 'footer',
      'priority' => 1
    ));

    $wp_customize -> add_setting('footer_text', array(
      'default' => _x('A35GAMES', 'Agames'),
      'type' => 'theme_mod'
    ));

    $wp_customize -> add_control('footer_text',array(
      'label' => __('Nome do Site', 'Agames'),
      'section' => 'footer',
      'priority' => 2
    ));
  }
  
  add_action('customize_register','load_customize_register');
