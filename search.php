<?php get_header(); ?>

    <div class="row">

      <div class="col-md-8 col-sm-12">
    
      <?php 
          // Se houver resultados exibe o conteúdo, se não exibe um formuládio de buscas
          if (is_search()) :
            $total_results = $wp_query->found_posts;

            echo "<h3 class='mb-3'>" . sprintf( __('%s resultado(s) para "%s"','Agames'), $total_results, get_search_query()) . "</h3>";

          endif;
          ?>

          <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

            <!-- Conteúdo de Posts -->
          <div class="blog-post">
            <h3 class="mb-3 pb-2 border-bottom"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="row">
              <div class="col-md-12 col-lg-6 mb-3">
                <a href="<?php the_permalink(); ?>">
                  <?php the_post_thumbnail('post-thumbnail',array('class' => 'img-fluid rounded')); ?>
                </a>
              </div>
              <div class="col-md-12 col-lg-6 mb-3 text-justify">
                <?php the_excerpt(); ?>
              </div>
            </div>
            <p class="text-muted">Por: <?php the_author(); ?> - em: <span class="badge badge-cor-3"><?php echo get_the_date('d/m/y'); ?></span></p>
          </div>

            <?php endwhile; ?>

          <?php else : 
          
          echo "<p>Sua busca não econtrou resultados. Use o formulário ao lado para refazer a busca.</p>";
            

          endif; ?>

          <div class="blog-pagination mb-3">
            <!-- Paginação de Posts -->
            <div class="row">
              <div class="text-left col-6">
              <?php previous_posts_link('<< Anterior'); ?>
              </div>
              <div class="text-right col-6">
              <?php next_posts_link('Próximo >>'); ?>
              </div>
            </div>
          </div>


      </div>

      <?php get_sidebar(); ?>

    </div>

  </div>

<?php get_footer(); ?>