<?php get_header(); ?>
<div class="container">
  <div class="row">

    <div class="col-md-8 col-sm-12">
      <!-- Loop de Posts -->
      <?php if(have_posts()) : while(have_posts()) : the_post();  ?>
      <!-- Imagende Destaque -->
      <?php the_post_thumbnail('post-thumbnail',array('class' => 'img-fluid rounded')); ?>

      <p class="text-muted mt-4">Por: <?php the_author(); ?> - em: <span
          class="badge badge-cor-3"><?php echo get_the_date('d/m/y'); ?></span></p>

      <h3 class="mb-3 border-top"><?php the_title(); ?></h3>
      <!-- Conteúdo de Posts -->

      <div class="mb-5 border-bottom text-justify">
        <?php the_content(); ?>
      </div>

      <?php comments_template(); ?>

      <?php endwhile; ?>

      <?php else : get_404_tempalte(); endif; ?>

    </div>

    <?php get_sidebar(); ?>

  </div>

</div>

<?php get_footer(); ?>