<?php get_header(); ?>

    <div class="row">

      <div class="col-md-8 col-sm-12">
        <h3 class="mb-3">Página não encontrada.</h3>

        <p>O caminho para o conteúdo que você procura está errado ou não existe.</p>

        <p>Realize uma nova busca para encontrar o que deseja.</p>
        

      </div>

      <?php get_sidebar(); ?>

    </div>

  </div>

<?php get_footer(); ?>