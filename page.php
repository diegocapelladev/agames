<?php get_header(); ?>
<div class="container">
  <div class="row">

    <div class="col-md-8 col-sm-12">
      <!-- Loop de Posts -->
      <?php if(have_posts()) : while(have_posts()) : the_post();  ?>
      <h3 class="mb-3"><?php the_title(); ?></h3>
      <!-- Conteúdo de Posts -->

      <div class="mb-5 text-justify">
        <?php the_content(); ?>
      </div>

      <?php endwhile; ?>

      <?php else : get_404_tempalte(); endif; ?>

    </div>

    <?php get_sidebar(); ?>

  </div>

</div>

<?php get_footer(); ?>