<footer>
  <div class="w-100 bg-secondary border-top border-dark mt-5">
    <div class="container">
      <div class="row">
        <div class="col py-3 text-center text-white">
          <h5 class="custom-footer">Desenvolvido por: <a
              href="https://www.linkedin.com/public-profile/settings?trk=d_flagship3_profile_self_view_public_profile"><?php echo get_theme_mod('footer_title', 'Diego Capella'); ?>
            </a> </h5>
          <p class="mb-0 custom-footer">&copy Copyright <?php echo date('Y - ')?> <a href="https://a35games.com/">
              <?php echo get_theme_mod('footer_text', 'A35GAMES'); ?></a> </p>
        </div>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/popper.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.js"></script>
</body>

</html>